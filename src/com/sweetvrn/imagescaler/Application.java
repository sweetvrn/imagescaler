package com.sweetvrn.imagescaler;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;

/**
 * Created by sweetvrn on 01.12.16.
 */
public class Application {


    public Application(String path, int width, int height){
        File scaledPathDir = new File(path+"/scaled");

        if (!scaledPathDir.exists() || !scaledPathDir.isDirectory()){
            if (!scaledPathDir.mkdir()) System.exit(255);
        }

        File[] images = findImages(path);
        for (File img : images){
            try {
                saveImage(scaleImage(img, width, height), img.getName(), scaledPathDir.getPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public Application(int width, int height){

        String path = System.getProperty("user.dir");
        File scaledPathDir = new File(path+"/scaled");

        if (!scaledPathDir.exists() || !scaledPathDir.isDirectory()){
            if (!scaledPathDir.mkdir()) System.exit(255);
        }

        File[] images = findImages(path);
        for (File img : images){
            try {
                saveImage(scaleImage(img, width, height), img.getName(), scaledPathDir.getPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private File[] findImages(String path){
        File root = new File(path);
        FilenameFilter filenameFilter = new FilenameFilter() {
            @Override
            public boolean accept(File file, String s) {
                String lowercaseName = s.toLowerCase();
                return lowercaseName.endsWith(".jpg")
                        ||lowercaseName.endsWith(".jpeg");
            }
        };
        return root.listFiles(filenameFilter);
    }

    private byte[] scaleImage(File imageFile, int width, int height) throws IOException {
        InputStream inputStream = new FileInputStream(imageFile);
        BufferedImage originalImage = ImageIO.read(inputStream);
        System.out.println("SCALING "+imageFile.getName()+" image...");
        if (originalImage == null) {
            System.out.println("error in "+imageFile.getName()+" image. Can't read");
            return null;
        }
        int originalWidth = originalImage.getWidth();
        int originalHeight = originalImage.getHeight();

        if (originalWidth > 0 && width > 0 && originalHeight > 0 && height > 0) {
            float ratio;
            Image image;
            BufferedImage changedImage;
            int deltaW = originalWidth - width;
            int deltaH = originalHeight - height;

            if (deltaW >= 0 && deltaW < deltaH) {    //Масштабируем по высоте
                ratio = (float) originalHeight / height;
                image = originalImage.getScaledInstance((int) (originalWidth / ratio), height, Image.SCALE_SMOOTH);
                changedImage = new BufferedImage((int) (originalWidth / ratio), height, BufferedImage.TYPE_INT_RGB);
            } else if (deltaH >= 0 && deltaH < deltaW) {    //Масштабируем по ширине
                ratio = (float) originalWidth / width;
                image = originalImage.getScaledInstance(width, (int) (originalHeight / ratio), Image.SCALE_SMOOTH);
                changedImage = new BufferedImage(width, (int) (originalHeight / ratio), BufferedImage.TYPE_INT_RGB);
            } else if (deltaH <= 0 || deltaW <= 0) {    //Не масштабируем. Исходные размеры меньше нужных
                System.out.println(imageFile.getName()+" - Не масштабируем. Исходные размеры меньше нужных");
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ImageIO.write(originalImage, "jpg", baos);
                baos.flush();
                byte[] bytes = baos.toByteArray();
                baos.close();
                return bytes;
            } else {      //Масштабируем как есть
                image = originalImage.getScaledInstance(width, height, Image.SCALE_SMOOTH);
                changedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            }
            Graphics2D g2d = changedImage.createGraphics();
            g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
            g2d.drawImage(image, 0, 0, null);
            g2d.dispose();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(changedImage, "jpg", baos);
            baos.flush();
            byte[] bytes = baos.toByteArray();
//            InputStream newImage = new ByteArrayInputStream(baos.toByteArray());
            baos.close();
            return bytes;
        } else {      //Отрицательных и нулевых размеров быть не должно
            return null;
        }
    }

    private void saveImage(byte[] bytes, String name, String path) throws IOException {
        FileOutputStream outputStream = new FileOutputStream(path+"/"+name);
        outputStream.write(bytes, 0, bytes.length);
        outputStream.close();
    }


    public static void main(String[] args) {
        String path;
        String width;
        String height;
        if (args.length <= 1){
            System.out.println("params: 1) path; 2) width (req); 3) height (req)");
        }else if (args.length == 2){
            System.out.println("found only two params. Path is root of app");
            width = args[0];
            height = args[1];
            new Application(Integer.parseInt(width), Integer.parseInt(height));
        }else{
            path = args[0];
            width = args[1];
            height = args[2];
            new Application(path, Integer.parseInt(width), Integer.parseInt(height));
        }

    }

}
